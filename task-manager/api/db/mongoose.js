const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/task-manager',{useNewUrlParser:true,useUnifiedTopology:true}).then(()=>{
    console.log('Mongodb connected successfully');
}).catch((e)=>{
    console.log("Error while connectiong to database");
    console.log(e);
});

//To preventd epreciation warnings
mongoose.set('useCreateIndex',true);
mongoose.set('useFindAndModify',false);

module.exports = {
    mongoose
};