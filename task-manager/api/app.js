const express = require('express');
const app = express();
const {mongoose} = require('./db/mongoose');
const bodyParser = require('body-parser');
//var cors = require('cors');
//load models
const {List,Task,User} = require('./db/models');
const jwt = require('jsonwebtoken');
const port = process.env.PORT || 5000;

//midddleware

//load middleware
app.use(bodyParser.json());
/* LIST ROUTES*/ 
//cors
//app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Methods", "HEAD,GET,POST,PATCH,OPTIONS,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    res.header(
        'Access-Control-Expose-Headers',
        'x-access-token','x-refresh-token'
    );
    next();
  });

//verify whether request has valid JWT access token
// let authenticate = (req,res,next) => {
//     let token = req.header('x-access-token');

//     //verify JWT
//     jwt.verify(token,User.getJWTSecret(),(err,decoded)=>{
//         if(err)
//         {
//             res.status(401).send(err);
//         }
//         else{
//             req.user_id=decoded._id;
//             next();
//         }
//     });
// }

  //verify refresh token which will be verifying session
  let verifySession = (req,res,next)=>{
      let refreshToken = req.header('x-refresh-token');
      let _id = req.header('_id');
      User.findByIdAndToken(_id,refreshToken).then((user)=>{
          if(!user){
              return Promise.reject({
                  'error':'User not found.Make sure that the resfresh-token and user are correct'
              })
          }

          req.user_id = user._id;
          req.userObject = user;
          req.refreshToken = refreshToken;

          let isSessionValid = false;
          user.sessions.forEach((session)=>{
              if(session.token === refreshToken){
                  if(User.hasRefreshTokenExpired(session.expiresAt)===false)
                  {
                        isSessionValid = true;
                  }
              }
          })
          if(isSessionValid)
          {
              next();
          }
          else
          {
              return Promise.reject({
                  'error':'Refresh token has expired or the session is invalid'
              })
          }
      }).catch((e)=>{
          res.status(401).send(e);
      })

}
//get/lists
app.get('/lists',(req,res)=>{
    //we want to return an array of lists in database
    List.find({
      // _userId : req.user_id
    }).then((lists)=>{
        res.send(lists);
    }).catch((e)=>{
        res.send(e);
    })
})

app.post('/lists',(req,res)=>{
    //we want to create a new list and return new list document
    let title=req.body.title;
    let newList = new List({
        title,
       // _userId:req.user_id
    });
    newList.save().then((listDoc)=>{
        res.send(listDoc);
    })
})
app.patch('/lists/:id',(req,res)=>{
    //update specify list with new values of JSON body
    List.findOneAndUpdate({_id:req.params.id},{
        $set:req.body
        }).then(()=>{
            res.sendStatus(200);
        })
})
app.delete('/lists/:id',(req,res)=>{
    //delete specified lists
    List.findByIdAndDelete({_id:req.params.id}).then((removedListDoc)=>{
            res.send(removedListDoc);
            //delete all the tasks that are in deleted list
            deleteTasksFromList(removedListDoc._id);
        })
})

//get lists/listid/tasks
app.get('/lists/:listId/tasks',(req,res)=>{
    Task.find({
        _listId:req.params.listId
    }).then((tasks)=>{
        res.send(tasks);
    })
});

app.get('/lists/:listId/tasks/:taskId',(req,res)=>{
    Task.findOne({
        _id:req.params.taskId,
        _listId:req.params.listId
    }).then((task)=>{
        res.send(task);
    })
})

//post list/listid/tasks
app.post('/lists/:listId/tasks',(req,res)=>{
    List.findOne({
        _id:req.params.listId,
      //  _userId:req.user_id
    }).then((list)=>{
        if(list){
            return true;
        }
        return false;
    }).then((canCreateTask)=>{
        if(canCreateTask)
        {
            let newTask = new Task({
                title:req.body.title,
                _listId:req.params.listId
            });
            newTask.save().then((newTaskDoc)=>{
                res.send(newTaskDoc);
            })
        }
        else{
            res.sendStatus(404);
        }
    })
    
})

//update tasks
app.patch('/lists/:listId/tasks/:taskId',(req,res)=>{
    //update specify list with new values of JSON body

    List.findOne({
        _id:req.params.listId,
        //_userId:req.user_id
    }).then((list)=>{
        if(list)
        {
            return true;
        }
        return false;
    }).then((canUpdateTask)=>{
        if(canUpdateTask)
        {
            Task.findByIdAndUpdate({_id:req.params.taskId,
                _listId:req.params.listId},
                {
                $set:req.body
                }).then(()=>{
                    res.send({message:'Updated Successfully!'});
                })
        }
        else{
            res.sendStatus(404);
        }
    })
    
})

//delete tasks

app.delete('/lists/:listId/tasks/:taskId',(req,res)=>{

    List.findOne({
        _id:req.params.listId,
       // _userId:req.user_id
    }).then((list)=>{
        if(list)
        {
            //delete specify list with new values of JSON body
            return true;
        }
        return false;
    }).then((canDeleteTask)=>{
        if(canDeleteTask)
        {
            Task.findOneAndRemove({_id:req.params.taskId,
                _listId:req.params.listId
                }).then((removedTaskDocs)=>{
                    res.send(removedTaskDocs);
                })
        }
        else{
            res.sendStatus(404);
        }
    })
    
})

//user routes

//post/signup

app.post('/users',(req,res)=>{
    let body = req.body;
    let newUser = new User(body);
    newUser.save().then(()=>{
        return newUser.createSession();
    }).then((refreshToken)=>{
        return newUser.generateAccessAuthToken().then((accessToken)=>{
            return {accessToken,refreshToken}
        });

    }).then((authTokens)=>{
        res
        .header('x-refresh-token',authTokens.refreshToken)
        .header('x-access-token',authTokens.accessToken)
        .send(newUser);
    }).catch((e)=>{
        res.status(400).send(e);
    })
})

//post - login route

app.post('/users/login',(req,res)=>{
    let email=req.body.email;
    let password = req.body.password;

    User.findByCredentials(email,password).then((user)=>{
        return user.createSession().then((refreshToken)=>{
            return user.generateAccessAuthToken().then((accessToken)=>{
                return {accessToken,refreshToken}
            })
        }).then((authTokens)=>{
            res
        .header('x-refresh-token',authTokens.refreshToken)
        .header('x-access-token',authTokens.accessToken)
        .send(user);
        }).catch((e)=>{
            res.status(400).send(e);
        })
    })
})
//delete request

app.delete('/users/:user_id',(req,res)=>{
    User.findOneAndRemove({_id:req.params.user_id,
        }).then((removedTaskDocs)=>{
            res.send(removedTaskDocs);
        })
})

//get request

app.get('/users',(req,res)=>{
    //we want to return an array of lists in database
    User.find({}).then((users)=>{
        res.send(users);
    }).catch((e)=>{
        res.send(e);
    })
})
app.get('/users/me/access-token',verifySession,(req,res)=>{
     req.userObject.generateAccessAuthToken().then((accessToken)=>{
         res.header('x-access-token',accessToken).send({accessToken});
     }).catch((e)=>{
         res.status(400).send(e);
     })
})

let deleteTasksFromList = (_listId) => {
    Task.deleteMany({
        _listId
    }).then(()=>{
        console.log("Tasks from" + _listId + " are deleted!");
    });
}
app.listen(5000,()=>{
    console.log(`Server started at port ${port}`);
})