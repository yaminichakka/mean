import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { List } from 'src/app/models/list.model';
import { Task } from 'src/app/models/task.model';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {

  lists:List[];
  tasks:Task[];

  constructor(private taskservice:TaskService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params:Params)=>{
       // console.log(params);
       if(params.listId)
       {
        this.taskservice.getTasks(params.listId).subscribe((tasks:Task[])=>{
          this.tasks = tasks;
        })
      }
      else{
        this.tasks = undefined;
      }
      }
    )
    this.taskservice.getLists().subscribe((lists:List[])=>{
      this.lists=lists;
    })
  }
  onTaskClick(task:Task)
  {
    //task to complete
    this.taskservice.complete(task).subscribe(()=>{
      console.log("Completed Successfully");
      //task set to be completed successfully
      task.completed = !task.completed;
    });
  }
}
