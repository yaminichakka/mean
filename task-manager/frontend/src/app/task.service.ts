import { Injectable } from '@angular/core';
import { Task } from './models/task.model';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private webReqservice:WebRequestService) { }

  getLists()
  {
    return this.webReqservice.get('lists');
  }
  createList(title:string){
    return this.webReqservice.post('lists',{title});
  }
  getTasks(listId:string)
  {
    return this.webReqservice.get(`lists/${listId}/tasks`)
  }
  createTask(title:string,listId:string){
    return this.webReqservice.post(`lists/${listId}/tasks`,{title});
  }
  complete(task:Task)
  {
    return this.webReqservice.patch(`lists/${task._listId}/tasks/${task._id}`,{
       completed:!task.completed
    });
  }
}
